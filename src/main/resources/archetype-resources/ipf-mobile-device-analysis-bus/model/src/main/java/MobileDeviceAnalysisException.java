package ${groupId-api}.model.exception;

public class MobileDeviceAnalysisException extends Exception {

    public MobileDeviceAnalysisException(String exceptionMessage){
        super(exceptionMessage);
    }

    public MobileDeviceAnalysisException(Exception exception){
        super(exception);
    }

    public MobileDeviceAnalysisException(String message, Exception exception){
        super(message, exception);
    }
}
