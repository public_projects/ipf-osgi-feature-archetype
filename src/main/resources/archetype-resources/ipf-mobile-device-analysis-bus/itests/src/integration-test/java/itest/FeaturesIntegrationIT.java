package itest;

import com.ipfli.ipf.runtime.test.config.CustomKarafTestConfiguration;
import com.ipfli.ipf.runtime.test.support.KarafTestBase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;
import static org.ops4j.pax.exam.CoreOptions.*;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class FeaturesIntegrationIT extends KarafTestBase {

    @Configuration
    public Option[] config() {
        return new CustomKarafTestConfiguration()
                       .addFeatureUrl("${groupId-api}.bus", "features")
                       .getCompleteTestConfiguration();

    }

    @Test
    public void shouldInstallModel() throws Exception {
        installAndAssertFeature("${artifactId}-model");
    }

    @Test
    public void shouldInstallApi() throws Exception {
        installAndAssertFeature("${artifactId}-api");
    }
}
