package ${groupId-api}.api.configuration;

/**
 * Managed service to read ${project-casual-prefix} related configuration
 */
public interface MobileDeviceAnalysisConfigurationService {
}
