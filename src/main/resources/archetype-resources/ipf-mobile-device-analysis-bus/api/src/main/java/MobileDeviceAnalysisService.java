package ${groupId-api}.api;

import ${groupId-api}.model.TargetMobileDeviceIdentityDetailModel;
import ${groupId-api}.model.exception.MobileDeviceAnalysisException;
import java.util.List;

public interface MobileDeviceAnalysisService {
    List<TargetMobileDeviceIdentityDetailModel> getUniqueIdentityCombination(String targetUUID,
                                                                        long fromDate,
                                                                        long toDate) throws MobileDeviceAnalysisException;
}
