package ${groupId-api}.web.api;

import com.ipfli.ipf.security.api.Constants;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/management/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@PreAuthorize(Constants.AUTHENTICATED)
public interface MobileDeviceIdentityWebService {

    /**
     * Gets details for a particular investigation.
     *
     * @param targetId The investigation id
     * @param fromDate The investigation id
     * @param toDate The investigation id
     * @return
     * <ul>
     *   <li>Status code 204, if no result found.</li>
     *   <li>Status code 200 with the result, if found Observation.</li>
     * </ul>
     */
    @GET
    @Path("{targetId}/device/registry/{fromDate}/{toDate}")
    Response getUniqueIdentityCombination(String targetId, long fromDate, long toDate);
}
