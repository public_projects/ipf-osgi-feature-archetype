package ${groupId-api}.core.internal;

import ${groupId-api}.api.MobileDeviceAnalysisService;
import ${groupId-api}.dao.api.TargetMobileDeviceIdentityDetailDAO;
import ${groupId-api}.model.TargetMobileDeviceIdentityDetailModel;
import ${groupId-api}.model.exception.MobileDeviceAnalysisException;

import java.util.List;

public class MobileDeviceAnalysisServiceImpl implements MobileDeviceAnalysisService {
    private TargetMobileDeviceIdentityDetailDAO targetMobileDeviceIdentityDetailDAO;

    public MobileDeviceAnalysisServiceImpl(TargetMobileDeviceIdentityDetailDAO targetMobileDeviceIdentityDetailDAO){
        this.targetMobileDeviceIdentityDetailDAO = targetMobileDeviceIdentityDetailDAO;
    }

    public List<TargetMobileDeviceIdentityDetailModel> getUniqueIdentityCombination(String targetUUID,
                                                                             long fromDate,
                                                                             long toDate) throws MobileDeviceAnalysisException {
        return null;
    }
}
