package ${groupId-api}.dao.api;

import ${groupId-api}.dao.api.model.TargetMobileDeviceIdentityDetail;
import com.ipfli.ipm.astyanax.dao.api.GenericColumnDAO;
import com.eaio.uuid.UUID;
import java.util.Map;

public interface TargetMobileDeviceIdentityDetailDAO extends GenericColumnDAO<UUID, Long, TargetMobileDeviceIdentityDetail> {
}
