package ${groupId-api}.dao.api.exception;

import com.ipfli.ipm.astyanax.dao.api.DAOException;

public class MobileDeviceDataAccessException extends DAOException {

    private final String message;

    public MobileDeviceDataAccessException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
