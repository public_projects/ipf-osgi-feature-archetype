package ${groupId-api}.core.internal;

import com.google.gson.InstanceCreator;
import com.ipfli.ipf.source.model.Source;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class TestSource implements InstanceCreator<Source>, Source {

    public TestSource() {}

    @Override
    public String getSystem() {
        return "test";
    }

    @Override
    public Map<String, String> getProperties() {
        return new HashMap<>();
    }

    @Override
    public Source createInstance(Type type) {
        return new TestSource();
    }
}
