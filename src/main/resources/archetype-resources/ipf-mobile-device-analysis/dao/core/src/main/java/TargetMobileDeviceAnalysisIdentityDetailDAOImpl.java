package ${groupId-api}.dao.core.internal;

import ${groupId-api}.dao.api.model.TargetMobileDeviceIdentityDetail;
import ${groupId-api}.dao.api.TargetMobileDeviceIdentityDetailDAO;
import com.ipfli.ipm.astyanax.dao.api.DAOException;
import com.ipfli.ipm.astyanax.dao.api.RecordNotFoundException;
import com.ipfli.ipm.astyanax.api.model.ColumnFamilyDefinition;
import com.ipfli.ipm.astyanax.api.model.DataType;
import com.ipfli.ipm.astyanax.dao.core.AbstractColumnDAO;
import com.ipfli.ipm.astyanax.extras.eaio.UUIDJavaType;
import com.ipfli.ipm.astyanax.extras.gson.GsonDataType;
import com.ipfli.ipm.astyanax.extras.gson.GsonFactory;
import com.eaio.uuid.UUID;
import com.netflix.astyanax.Keyspace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TargetMobileDeviceAnalysisIdentityDetailDAOImpl extends AbstractColumnDAO<UUID, Long, TargetMobileDeviceIdentityDetail>
        implements TargetMobileDeviceIdentityDetailDAO {
    private static final String MOBILE_DEVICE_ANALYSIS = "DeviceAnalysis_Lookup";
    private Logger LOGGER = LoggerFactory.getLogger(TargetMobileDeviceAnalysisIdentityDetailDAOImpl.class);

    public TargetMobileDeviceAnalysisIdentityDetailDAOImpl(final Keyspace keyspace, final GsonFactory factory) {
        super(keyspace, createColumnFamily(factory));
    }

    private static ColumnFamilyDefinition<UUID, Long, TargetMobileDeviceIdentityDetail> createColumnFamily(GsonFactory gsonFactory) {
        return new ColumnFamilyDefinition<>(MOBILE_DEVICE_ANALYSIS,
                                            new UUIDJavaType(false),
                                            DataType.LONG,
                                            GsonDataType.of(TargetMobileDeviceIdentityDetail.class, gsonFactory));
    }
}
