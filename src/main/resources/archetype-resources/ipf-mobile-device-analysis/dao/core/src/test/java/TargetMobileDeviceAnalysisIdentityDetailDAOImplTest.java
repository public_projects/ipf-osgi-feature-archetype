package ${groupId-api}.core.internal;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.source.model.Source;

import com.ipfli.ipm.astyanax.extras.gson.GsonFactory;
import com.ipfli.ipm.astyanax.extras.gson.internal.DynamicGsonFactory;
import com.ipfli.ipm.astyanax.test.CassandraTestBase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TargetMobileDeviceAnalysisIdentityDetailDAOImplTest extends CassandraTestBase {
    private final static Logger LOGGER = LoggerFactory.getLogger(TargetMobileDeviceAnalysisIdentityDetailDAOImplTest.class);
    private GsonFactory gsonFactory;
    private Source source;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSaving(){

    }
}
